// function mycarousel_initCallback(carousel)
// {
    // // Disable autoscrolling if the user clicks the prev or next button.
    // carousel.buttonNext.bind('click', function() {
        // carousel.startAuto();
    // });

    // carousel.buttonPrev.bind('click', function() {
        // carousel.startAuto();
    // });

    // // Pause autoscrolling if the user moves with the cursor over the clip.
    // carousel.clip.hover(function() {
        // carousel.stopAuto();
    // }, function() {
        // carousel.startAuto();
    // });
// }

var jRes = jRespond([
    {
        label: 'handheld',
        enter: 0,
        exit: 767
    }
		,
		{
        label: 'tablet',
        enter: 768,
        exit: 979
    },
		{
			label: 'laptop',
        enter: 980,
        exit: 10000
    }
]);

jQuery(document).ready(function() {
    
    
    jQuery(document).scroll(function(e){
     var current_scroll_top = jQuery(document).scrollTop();
      if(current_scroll_top > 350){
        jQuery(".header-menu").addClass('fixed');
      }
     else{
      jQuery(".header-menu").removeClass('fixed');
     }
    });
	
	jQuery(".running-events .views-field-title").click(function(){
	jQuery(this).parent().find(".views-field-field-photo").slideToggle();
	  jQuery(this).parent().find(".views-field-field-body").slideToggle();
	  
	});
		
	jQuery(".region-front-content .block .item-list ul").click(function(){
	var lnk = jQuery(this).parent().parent().parent().parent().parent().find("h2.title a").attr('href');
		location.href = lnk;
	});
		
		
		
		
		var searchbox = jQuery('#block-search-form').clone();
		jRes.addFunc({
			breakpoint: 'tablet',
				enter: function() {
					/* Product Listing Page */
					jQuery(".region.region-logo-banner").prepend(searchbox);
					jQuery('.region.region-menu-block .block.block-search').remove();
				},
				exit: function() {
					jQuery(".region.region-menu-block").append(searchbox);
					jQuery(".region.region-logo-banner .block.block-search").remove();	
				}
		});
		
		var searchbox = jQuery('#block-search-form').clone();
		jRes.addFunc({
			breakpoint: 'handheld',
				enter: function() {
					/* Product Listing Page */
					jQuery(".region.region-logo-banner").prepend(searchbox);
					jQuery('.region.region-menu-block .block.block-search').remove();
				},
				exit: function() {
					jQuery(".region.region-menu-block").append(searchbox);
					jQuery(".region.region-logo-banner .block.block-search").remove();	
				}
		});
		
		
		
		
		

    jQuery("#node-78").parent().parent().parent().prev().hide();
    
    var print_code = jQuery(".region-body-inner .node-page .links.inline").html();
    if(print_code!=null)
    {
        jQuery(".region-body-inner .node-page .links.inline").remove();
        jQuery(".region-body-inner").append("<ul class='links inline'>"+print_code+"</ul>")
    }    
    
    jQuery(".each-video a").click( function(){
        
        jQuery(this).parent().children('.overlay-video').fadeIn(400);
        jQuery(this).parent().children('.video-popup').fadeIn(500);
        
        return false;
    });
    
    jQuery(".overlay-video").click( function(){
        jQuery(this).fadeOut(500);        
        jQuery(this).prev().fadeOut(400);
        
        var iFrame = jQuery(this).prev().html();
        jQuery(this).prev().children().remove();
        jQuery(this).prev().html(iFrame);
        
    });
    
//==============================================Change Downladable file Text==================================================//
if (jQuery('div').hasClass('field-type-file')) {
	jQuery('.field-items span.file a').text('Download Here');
}

//==============================================Jcarosal==================================================//
// if ($('div').hasClass('caro-container')) {
	// jQuery('.jcarousel-skin-tango').jcarousel({
        // auto: 6,
        // wrap: 'circular',
        // initCallback: mycarousel_initCallback
    // });
// }
	/*$('#news-slide ul.slider-main2').hover(function() {
		$('.news-slide-navigator-wapper .news-slide-pause').trigger('click'); 
    },
    function() {
		$('.news-slide-navigator-wapper .news-slide-next').trigger('click'); 
    });*/

// End jQuery

});
