from django.urls import path
from . import views

urlpatterns = [
    path('', views.home, name='home'),
    path('about', views.about, name='about'),
    path('application', views.application, name='application'),
    path('case', views.case, name='case'),
    path('contact', views.contact, name='contact'),
    path('renew', views.renew, name='renew'),

]
