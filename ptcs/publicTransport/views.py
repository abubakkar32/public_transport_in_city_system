from django.shortcuts import render


# Create your views here.
def home(request):
    return render(request, 'Transport/Home.html')


def about(request):
    return render(request, 'Transport/About.html')


def application(request):
    return render(request, 'Transport/Application.html')


def case(request):
    return render(request, 'Transport/Case.html')


def contact(request):
    return render(request, 'Transport/Contact.html')


def renew(request):
    return render(request, 'Transport/Renew.html')
