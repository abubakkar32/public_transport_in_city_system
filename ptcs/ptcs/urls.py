from django.contrib import admin
from django.urls import path, include
from crud import views


urlpatterns = [
    path('admin/', admin.site.urls),
    path('index', views.show),
    path('show', views.show),
    path('edit/<int:id>', views.edit),
    path('update/<int:id>', views.update),
    path('delete/<int:id>', views.destroy),
    path('account/', include('accounts.urls')),
    path('', include('publicTransport.urls'))

]
